#!/usr/bin/make -f

export SBTHOME=$(CURDIR)/debian/sbt_home
export HOME=$(SBTHOME)
export SBTCMD=$(CURDIR)/bootstrapsbt/bin/sbt -verbose -debug -sbt-dir $(SBTHOME)/.sbt -ivy $(SBTHOME)/.ivy2

PKD    = $(abspath $(dir $(MAKEFILE_LIST)))
SRCPKG = $(word 2,$(shell dpkg-parsechangelog -l$(PKD)/changelog | grep ^Source))
BINPKG = lib$(SRCPKG)-java
PKGVER   ?= $(shell dpkg-parsechangelog -l$(PKD)/changelog -SVersion | cut -d- -f1)
SBTVER = $(shell grep sbt.version= $(CURDIR)/project/build.properties | sed 's/.*=//')
URL    = https://dl.bintray.com/sbt/native-packages/sbt/$(SBTVER)/sbt-$(SBTVER).tgz
LURL   = https://github.com/sbt/sbt-launcher-package.git
UDATE  = $(shell date --rfc-3339=seconds --date='TZ="UTC" $(shell echo $(PKGVER) | perl -ne 'print "$$1-$$2-$$3" if m/\+(?:git|svn|hg)(\d{4})(\d{2})(\d{2})/')')
LVER   = $(shell grep 'version :=' ./build.sbt | sed 's/.*"\([^"]*\)".*/\1/')

PKG_ORIG = $(CURDIR)/../$(SRCPKG)_$(PKGVER).orig.tar.gz
BOOTSTRAP_SBT = $(CURDIR)/../$(SRCPKG)_$(PKGVER).orig-bootstrapsbt.tar.xz
BOOTSTRAP_DEPS = $(CURDIR)/../$(SRCPKG)_$(PKGVER).orig-bootstrapdeps.tar.xz
BOOTSTRAP_DEPS_SOURCES = $(CURDIR)/../$(SRCPKG)_$(PKGVER).orig-bootstrapdeps-sources.tar.xz
SBT_LAUNCHER = $(CURDIR)/../$(SRCPKG)_$(PKGVER).orig-launcher.tar.xz

%:
	dh $@ --with javahelper --with maven_repo_helper

override_dh_auto_build:
	git init
	#rm bootstrapdeps/org.scala-sbt/launcher/jars/launcher-1.0.0-M1.jar
	mkdir -p $(SBTHOME)/.ivy2/cache
	cp -fr $(CURDIR)/bootstrapdeps/* $(SBTHOME)/.ivy2/cache/
	mkdir $(SBTHOME)/.sbt
	echo "[repositories]" > $(SBTHOME)/.sbt/repositories
	#echo "debian-local-maven: file:///usr/share/maven-repo" >> $(SBTHOME)/.sbt/repositories
	echo "local" >> $(SBTHOME)/.sbt/repositories
	cp -frv $(CURDIR)/bootstrapdeps-sources/. $(SBTHOME)/.ivy2/cache/
	$(SBTCMD) 'set offline := true' -Dsbt.override.build.repos=true packageBin make-pom
	find ./ -name '*.pom'|grep target|sed 's/\(.*\).pom/\1.pom --package=$(BINPKG) --has-package-version --java-lib --artifact=\1.jar/' |\
		grep -v sbtroot- |\
		sed 's/sbt-launch-[^ ]*.jar/sbt-launch.jar/' > $(CURDIR)/debian/$(BINPKG).poms

override_dh_clean:
	rm -fr .git $(SBTHOME) project/project $(shell find ./ -name target -type d) debian/$(BINPKG).poms
	dh_clean

override_dh_install:
	dh_install
	echo "-sbt-jar /usr/share/java/sbt-launch-$(LVER).jar" >> $(CURDIR)/debian/sbt/usr/share/sbt/conf/sbtopts

.PHONY: get-orig-source

get-orig-source: $(PKG_ORIG) $(BOOTSTRAP_DEPS) $(BOOTSTRAP_DEPS_SOURCES) $(SBT_LAUNCHER)

$(PKG_ORIG):
	@echo
	@echo "Downloading latest $(SRCPKG)-$(PKGVER)"
	uscan --noconf --verbose --rename --check-dirname-level=0 --download-version $(PKGVER) $(PKD)

$(BOOTSTRAP_SBT):
	@echo
	@echo "Fetching sbt binary for bootstrap"
	wget -q $(URL)
	rm -fr bootstrapsbt
	mkdir bootstrapsbt
	tar -C bootstrapsbt --strip-components=1 -zxf sbt-$(SBTVER).tgz
	tar -JcC bootstrapsbt -f $(BOOTSTRAP_SBT) .
	rm -fr sbt-$(SBTVER).tgz

$(BOOTSTRAP_DEPS) $(BOOTSTRAP_DEPS_SOURCES): $(BOOTSTRAP_SBT)
	@echo
	@echo "Fetching online sbt project build dependancies"
	rm -fr bootstrapsbt
	mkdir bootstrapsbt bootstrapdeps-sources
	tar -Jx -C bootstrapsbt -f $(BOOTSTRAP_SBT)
	mkdir -p $(SBTHOME)/.ivy2/cache
	mkdir $(SBTHOME)/.sbt
	$(SBTCMD) update
	# "update" compiles compiler-interface jar with the scala defined by
	# the project, so we remove it and store all jar srcs into a dedicated
	# debian source component that will be copied before compiling the
	# whole project, including the compiler-interface jar
	# After bootstrap, we intend to provide this compiler-interface, built
	# with debian's scala
	rm -fr $(SBTHOME)/.ivy2/cache/org.scala-sbt/org.scala-sbt-compiler-interface-*
	# .properties and .xml.original will be regenerated and that will avoid unnecessary debian/copyright work
	find $(SBTHOME)/.ivy2/cache \( -name '*.properties' -o -name '*.xml.original' \) -delete
	# remove path with no jar file
	find $(SBTHOME)/.ivy2/cache/ -name '*.xml' |while read DIR; do \
		if [ ! -d $${DIR%/*}/jars -a ! -d $${DIR%/*}/bundles -a ! -d $${DIR%/*}/orbits -a ! -d $${DIR%/*}/eclipse-plugins ]; then rm -fr $${DIR%/*}; fi done
	cd $(SBTHOME)/.ivy2/cache ; find . -type d -name 'srcs'| while read DIR; do \
       		mkdir -p $(CURDIR)/bootstrapdeps-sources/$$DIR ; \
		mv $$DIR/* $(CURDIR)/bootstrapdeps-sources/$$DIR ; \
		rm -fr $$DIR ; \
		done
	tar -JcC $(SBTHOME)/.ivy2/cache/ -f $(BOOTSTRAP_DEPS) .
	tar -Jc -f $(BOOTSTRAP_DEPS_SOURCES) bootstrapdeps-sources

$(SBT_LAUNCHER):
	@echo
	@echo "Fetching sbt launcher package"
	$(if $(wildcard launcher),$(error launcher directory exist, aborting..))
	@echo "# Downloading..."
	git clone $(LURL) launcher \
	|| $(RM) -r launcher
	cd launcher \
	&& git checkout $$(git log -n1 --format=%h --before="$(UDATE)") \
	&& [ -s ChangeLog ] || ( echo "# Generating ChangeLog..." \
	; git log --pretty="format:%ad  %aN  <%aE>%n%n%x09* %s%n" --date=short > ChangeLog \
	; touch -d "$$(git log -1 --format='%ci')" ChangeLog) \
	&& echo "# Setting times..." \
	&& git ls-tree -r --name-only HEAD | while read F ; do touch --no-dereference -d "$$(git log -1 --format="%ai" -- "$$F")" "$$F"; done \
	&& echo "# Cleaning-up..." \
	&& $(RM) -r debian/ \
	&& $(RM) -r .git .git*
	@echo "# Packing..."
	tar -JcC launcher -f $(SBT_LAUNCHER) .
	#find -L "launcher" -xdev -type f -print | LC_ALL=C sort \
	#| XZ_OPT="-6v" tar -caf $(SBT_LAUNCHER) -T- --owner=root --group=root --mode=a+rX \
	#&& $(RM) -r "launcher"
